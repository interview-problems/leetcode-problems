import java.util.*;

public class UnableToEat {
    public static int countStudents(int[] students, int[] sandwiches) {
        int i = 0;
        int j = 0;
        int count = 0;
        Queue<Integer> queue = new LinkedList<>();

        for (Integer element : students) {
            queue.add(element);
        }
        while(i<queue.size())
        {
            System.out.println(queue);
            if(queue.peek() == sandwiches[j])
            {
                count++;
                i = 0;
                j++;
                queue.remove();
            }
            else
            {
                i++;
                queue.add(queue.remove());
            }
        }

        return students.length - count;
    }

    public static void main(String[] args)
    {
        int[] students = {1,1,1,0,0,1};
        int[] sandwiches = {1,0,0,0,1,1};
        //int[] students = {1,1,0,0};
        //int[] sandwiches = {0,1,0,1};
        int num = countStudents(students, sandwiches);
        System.out.println(num);
    }
}
