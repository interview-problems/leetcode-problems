import java.util.HashMap;
import java.util.Map;

class Solution {
    public static void main(String[] args) {
        String s = "XXVII";
        HashMap<Character, Integer> romanMap = getRomanMap();
        int prevNum = 9999;
        int sum = 0;
        for(int i =0; i<s.length(); i++)
        {
            int num = romanMap.get(s.charAt(i));
            if(prevNum < num)
            {
                sum = sum + num - 2*prevNum;
            }
            else {
                sum = sum + num;
            }
            prevNum = num;
        }

        System.out.println(sum);
    }

    public static HashMap<Character, Integer> getRomanMap()
    {
        Map<Character, Integer> romanMap = new HashMap<>();
        romanMap.put('I', 1);
        romanMap.put('V', 5);
        romanMap.put('X', 10);
        romanMap.put('L', 50);
        romanMap.put('C', 100);
        romanMap.put('D', 500);
        romanMap.put('M', 1000);

        return (HashMap<Character, Integer>) romanMap;
    }
}