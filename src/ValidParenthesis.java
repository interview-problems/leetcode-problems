public class ValidParenthesis {
    static final int MAX_SIZE = 101;
    static char[] a = new char[MAX_SIZE];
    static int top = -1;

    static void push(char ele)
    {
        if(top<=MAX_SIZE-1)
        {
            top++;
            a[top] = ele;
        }else {
            System.out.println("Stack is overflowed");
        }
    }

    static char pop()
    {
        if(top>=0)
        {
            char ele = a[top];
            top--;
            return ele;
        }
        else {
            System.out.println("Stack is underflowed");
            return '-';
        }
    }

    static boolean isEmpty()
    {
        return top == -1;
    }

    static boolean isFull()
    {
        return top >= MAX_SIZE;
    }

    static boolean isMatchingPair(char open, char close)
    {
        return open == '(' && close == ')';
    }

    static boolean isBalanced(String str)
    {
        for(int i = 0; i<str.length(); i++)
        {
            char ch = str.charAt(i);
            if(ch == '(')
            {
                push(ch);
            }
            else if(ch == ')')
            {
                if(isEmpty())
                {
                    return false;
                }
                char topChar = pop();
                if(!isMatchingPair(topChar, ch))
                {
                    return false;
                }
            }
        }
        return isEmpty();
    }
    public static void main(String[] args)
    {
        String expression = "((()))";
        //String expression = "(()))";
        if(isBalanced(expression))
        {
            System.out.println("Given expression: " + expression + " has balanced parenthesis");
        }
        else
        {
            System.out.println("Given expression: " + expression + " does not contain balanced paranthesis!");
        }
    }
}
