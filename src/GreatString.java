import java.util.*;

class GreatString {
    public String makeGood(String s) {
        Character[] chars = s.chars().mapToObj(c -> (char)c).toArray(Character[]::new);
        Stack<Character> stack = new Stack<>();
        for (Character aChar : chars) {
            if (stack.empty()) {
                stack.push(aChar);
            } else {
                int charInt = (int) aChar;
                char prevChar = stack.lastElement();
                if (charInt > 90 && charInt - 32 == (int) prevChar) {
                    stack.pop();
                } else if (charInt < 91 && charInt + 32 == (int) prevChar) {
                    stack.pop();
                } else {
                    stack.push(aChar);
                }
            }
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (Object character : stack) {
            stringBuilder.append(character);
        }


        return stringBuilder.toString();
    }

    public static void main(String[] args)
    {
        GreatString great = new GreatString();
        String greatString = great.makeGood("abBAcC");
        System.out.println(greatString);
    }
}